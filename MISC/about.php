<html>
  <head>
    <title>C&R Furniture</title>
    <link rel="stylesheet" href="/ASSETS/styles.css">
    <link rel="stylesheet" href="/ASSETS/nav.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Pacifico&display=swap" rel="stylesheet"> 
  </head>
  <body>
    <div class="container">
      <header>
        <a href="/index.php"><img src="/IMAGES/R&C.svg" alt="C&R Furniture" style="height:90px;"></a>
      </header>  
      <nav>
        <ul>
          <li><a href="/index.php">Home</a></li>
          <li class="active"><a href="/about.php">About</a></li>
          <li><a href="/MISC/contact.php">Contact</a></li>
        </ul>
      </nav>
      <main>Main</main>
      <div id="content1">Content1</div>
      <div id="content2">Content2</div>
      <div id="content3">Content3</div>
      <footer>Footer</footer>
    </div>
  </body>
</html>