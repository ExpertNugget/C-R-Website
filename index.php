<html>
<head>
    <title>C&R Furniture</title>
    <link rel="stylesheet" href="/ASSETS/styles.css">
    <link rel="stylesheet" href="/ASSETS/nav.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Pacifico&display=swap" rel="stylesheet"> 
  </head>
  <body>
    <div class="container">
      <header>
        <a href="/index.php"><img src="/IMAGES/R&C.svg" alt="C&R Furniture" style="height:40px;"></a>
        <h1>R&C Furniture</h1>
      </header>  
      <nav>
        <ul>
          <li class="active"><a href="/index.php">Home</a></li>
          <li><a href="/MISC/about.php">About</a></li>
          <li><a href="/MISC/contact.php">Contact</a></li>
        </ul>
      </nav>
      <main>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Fugit maiores officia ab, a dicta harum quisquam mollitia illo molestiae! Quos facilis impedit suscipit ipsum vel harum exercitationem reiciendis dignissimos culpa?</main>
      <div id="content1">Lorem ipsum dolor sit amet consectetur adipisicing elit. Corrupti soluta suscipit fugit error minima repellat praesentium ab beatae, adipisci nobis ullam voluptas sapiente itaque, magnam eligendi sit commodi autem, est quod id necessitatibus saepe deserunt vero facilis? Doloremque quibusdam enim consectetur adipisci nisi vitae optio tenetur ipsam, ipsum earum architecto consequuntur, accusamus aperiam quaerat natus totam eveniet aliquid consequatur iure? Nesciunt, voluptates sequi voluptatibus soluta illum aliquam quaerat ab aspernatur alias accusantium doloribus consequatur odit animi nostrum numquam modi rem a repellat explicabo dolorem. Quibusdam in qui ratione, quisquam beatae eligendi rerum fuga! Labore iusto quas quia atque provident veniam.</div>
      <div id="content2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi dolor harum, culpa consectetur pariatur enim corrupti deleniti quis nobis necessitatibus laboriosam libero optio vero quod. Quod molestias tempora, minus consequatur, cum labore impedit aliquid vero quae error deserunt at illum numquam porro! Architecto facilis iusto ad, voluptatum cum dolore reprehenderit, ab minima assumenda explicabo eligendi. Dolore minima assumenda repudiandae neque consequuntur consectetur quaerat tenetur, modi aliquam accusantium ea libero beatae saepe cupiditate reprehenderit facilis expedita iste ipsa nulla ratione eum. Facere nobis esse autem expedita vero veniam quidem tempora nam harum soluta, ab dicta tempore optio earum cupiditate quia temporibus.</div>
      <div id="content3">Lorem ipsum dolor sit amet consectetur adipisicing elit. Expedita consequatur architecto neque quod modi fugit officia dolore perspiciatis possimus libero accusantium, cupiditate, non delectus nihil dolor illo aliquid, ab eum?</div>
      <footer>Lorem ipsum dolor sit amet.</footer>
    </div>
  </body>
</html>